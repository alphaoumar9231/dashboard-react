import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Layout from './Component/Layout/Layout';
import Dashboard from './Pages/Dashboard/Dashboard';
import Index from './Pages/Index/Index';
import Create from './Pages/Create/Create';
import Edit from './Pages/Edit/Edit';
import Show from './Pages/Show/Show';
import Home from './Pages/Home/Home';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />

      <Route path='/dashboard' element={<Layout />}>
        <Route path="/dashboard/vente" element={<Dashboard />} />
        <Route path="/dashboard/:name" element={<Index />} />
        <Route path="/dashboard/:name/create" element={<Create />} />
        <Route path="/dashboard/:name/edit/:id" element={<Edit />} />
        <Route path="/dashboard/:name/show/:id" element={<Show />} />
      </Route>
    </Routes>
  );
}

export default App;
