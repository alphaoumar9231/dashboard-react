import React from 'react'
import { Link } from 'react-router-dom'

export default function Home() {
  return (
    <div>
      <Link to='/dashboard'>Aller a la page d'acceuil</Link>
    </div>
  )
}
