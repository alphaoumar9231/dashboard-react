import React from 'react'
import Cards from '../../Component/Cards/Cards'
import './Dashboard.css'
import { MdDeleteOutline, MdTabUnselected } from 'react-icons/md'
import { AiOutlinePlusCircle } from 'react-icons/ai'
import { GrSubtractCircle } from 'react-icons/gr'

export default function Dashboard() {
	return (
		<div>
			<h3>Section de vente</h3>
			<div className="container-fluid py-3">
				<div className="row">
					<div className="col-xl-7 m-0 bg-light col-lg-12 p-0 ventePrd">
						<div className='sticky-top hautProd mb-2'>
							<h4 className='text-center headerPro py-2'>Produit</h4>
							<div className='d-flex gap-2 px-2'>
								<select class="form-select" aria-label="Default select example">
									<option value="">Selection une categorie <MdTabUnselected /> </option>
									<option value="">Legume</option>
									<option value="">fruits</option>
									<option value="">Huile</option>
									<option value="">Riz</option>
								</select>
								<input type="search" className='form-control vw-50' placeholder='Chercher un produit' />
							</div>
						</div>
						<Cards className=" mt-3" />
					</div>
					<div className="col-xl-5 col-lg-12 coteFAC">
						<div className='venteFac'>
							<div className='sticky-top hautProd'>
								<h4 className='text-center headerPro py-2'>Facture</h4>
								<div className="d-flex px-1 gap-2">
									<select class="form-select" aria-label="Default select example">
										<option selected>Selection un client</option>
										<option value="1">One</option>
										<option value="2">Two</option>
										<option value="3">Three</option>
									</select>
									<div>
										<button className='d-flex align-items-center text-white fw-semibold btn btn-info'>
											<AiOutlinePlusCircle /> Client
										</button>
									</div>
								</div>
							</div>
							<div  >
								<div class="table-responsive mt-2">
									<table class="table table-bordered table-hover">
										<thead class="table-dark">
											<tr>
												<th scope="col">Nom</th>
												<th scope="col">Quantité</th>
												<th scope="col">Prix</th>
												<th scope="col">Supprimé</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row">Test</th>
												<td>
													<GrSubtractCircle className=' border hover-danger fs-2 p-1  text-white  border-danger rounded facbtnSup' />
													<span className='fs-5 fw-bolder'> 1 </span>
													<AiOutlinePlusCircle className=' border hover-danger fs-2 p-1  text-primary  border-primary rounded facbtnAdd' />
												</td>
												<td>20</td>
												<td className='text-center'>
													<button className='btn '>
														<MdDeleteOutline className=' border hover-danger fs-2 p-1  text-danger  border-danger rounded facbtnSup' />
													</button>
												</td>
											</tr>
											<tr>
												<th scope="row">Riz</th>
												<td>
													<GrSubtractCircle className=' border hover-danger fs-2 p-1  text-white  border-danger rounded facbtnSup' />
													<span className='fs-5 fw-bolder'> 1 </span>
													<AiOutlinePlusCircle className=' border hover-danger fs-2 p-1  text-primary  border-primary rounded facbtnAdd' />
												</td>
												<td>100</td>
												<td className='text-center'>
													<button className='btn '>
														<MdDeleteOutline className=' border hover-danger fs-2 p-1  text-danger  border-danger rounded facbtnSup' />
													</button>
												</td>
											</tr>
											<tr>
												<th scope="row">Mangue</th>
												<td>
													<GrSubtractCircle className=' border hover-danger fs-2 p-1  text-white  border-danger rounded facbtnSup' />
													<span className='fs-5 fw-bolder'> 1 </span>
													<AiOutlinePlusCircle className=' border hover-danger fs-2 p-1  text-primary  border-primary rounded facbtnAdd' />
												</td>
												<td >300</td>
												<td className='text-center'>
													<button className='btn '>
														<MdDeleteOutline className=' border hover-danger fs-2 p-1  text-danger  border-danger rounded facbtnSup' />
													</button>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div className='d-flex justify-content-evenly'>
									<p>Prix total :</p> <p>3000f</p>
								</div>
								<div className='d-flex justify-content-evenly py-2'>
									<button className='btn btn-danger'>Annuler</button>
									<button className='btn btn-success'>Valider</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	)
}
