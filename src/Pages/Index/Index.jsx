import React from 'react'
import './Index.css'
import { AiFillPlusCircle } from 'react-icons/ai'
import { BiSearch } from 'react-icons/bi'
import { FaEdit } from 'react-icons/fa'
import { BiShow } from 'react-icons/bi'
import { RiDeleteBin6Line } from 'react-icons/ri'
import { Link, useParams } from 'react-router-dom'

export default function Index() {
	const { name } = useParams();
	const id = 1;
	return (
		<div>
			<h1>Facture de ventes</h1>
			<p>Créez, consultez et gérez vos enrégistrements</p>
			<div className="d-flex justify-content-between align-items-center px-3" >
				<div class="input-group mb-3 recherche">
					<span class="input-group-text" id="basic-addon1"><BiSearch /></span>
					<input type="text" class="form-control" placeholder="Chercher" aria-label="Chercher une facture" aria-describedby="basic-addon1" />
				</div>
				<Link to={`/dashboard/${name}/create`} className='d-flex fs-4 justify-content-center gap-2 btnAjout align-items-center'>
					Facture <AiFillPlusCircle className='fs-2 pt-2' />
				</Link>
			</div>
			<div className="table-responsive ">
				<table class="table table-bordered table-hover bg-white">
					<thead className='table-dark'>
						<tr>
							<th scope="col">#</th>
							<th scope="col">First</th>
							<th scope="col">Last</th>
							<th scope="col">Action </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">1</th>
							<td>Mark</td>
							<td>Otto</td>
							<td className="d-flex gap-4 fs-4 pb-1 justify-content-center  actionEDS" >
								<Link to={`/dashboard/${name}/show/${id}`} className='description'>
									<BiShow /><span className='cription'>Details</span>
								</Link>
								<button className='description'>
									<RiDeleteBin6Line /> <span className='cription'>Supprimer</span>
								</button >
								<Link to={`/dashboard/${name}/edit/${id}`} className='description'>
									<FaEdit /> <span className='cription'>Editer</span>
								</Link>
							</td>
						</tr>
						<tr>
							<th scope="row">2</th>
							<td>Jacob</td>
							<td>Thornton</td>
							<td className="d-flex gap-4 fs-4 pb-1  justify-content-center  actionEDS" >
								<Link to={`/dashboard/${name}/show/${id}`} className='description'>
									<BiShow /><span className='cription'>Details</span>
								</Link>
								<button className='description'>
									<RiDeleteBin6Line /> <span className='cription'>Supprimer</span>
								</button >
								<Link to={`/dashboard/${name}/edit/${id}`} className='description'>
									<FaEdit /> <span className='cription'>Editer</span>
								</Link>
							</td>
						</tr>
						<tr>
							<th scope="row">3</th>
							<td >Larry the Bird</td>
							<td >Larry the Bird</td>
							<td className="d-flex gap-4 fs-4 pb-1 justify-content-center  actionEDS" >
								<Link to={`/dashboard/${name}/show/${id}`} className='description'>
									<BiShow /><span className='cription'>Details</span>
								</Link>
								<button className='description'>
									<RiDeleteBin6Line /> <span className='cription'>Supprimer</span>
								</button >
								<Link to={`/dashboard/${name}/edit/${id}`} className='description'>
									<FaEdit /> <span className='cription'>Editer</span>
								</Link>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	)
}
