import "./ProfilNavBar.css";
import avatar from '../../images/avatar.png';
import React, { useEffect, useState } from "react";
import { FaUserAlt } from "react-icons/fa";
import { BiLogOutCircle } from "react-icons/bi";
import { Link, useNavigate } from "react-router-dom";
import { Button, Dropdown, ButtonGroup } from 'react-bootstrap';


function ProfilNavBar() {
 

  return (
    <Dropdown className="dropNAvbar" as={ButtonGroup}>
      <Dropdown.Toggle id="dropdown-custom-1">
        <img src={avatar} alt='zlogo' className='imgNavBar' />
      </Dropdown.Toggle>
      <Dropdown.Menu className="super-colors ">
        <h6 className="text-center">User option</h6>
        <Dropdown.Item eventKey="1" className="text-center">
          <Link > <FaUserAlt /> Profile</Link>
        </Dropdown.Item>
        <Dropdown.Item eventKey="2">
          <Button className="bDprdNb" > <BiLogOutCircle /> Deconnexion</Button>
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default ProfilNavBar;
