import "./SideBar.css";
import React from "react";
import { NavLink } from "react-router-dom";
import { AiFillDashboard } from 'react-icons/ai'
import { GiBookPile } from 'react-icons/gi'
import { RiNumber1, RiNumber2 } from 'react-icons/ri'

function SideBar() {
  const name = 'produits'
  return (
    <div className="side-bar">
      <NavLink to='/dashboard/vente' className="d-block">
        <AiFillDashboard /> <span className="ms-1 d-none d-md-inline">Dashboard</span>
      </NavLink>
      <NavLink to={`/dashboard/produits`} className="d-block">
        <GiBookPile /> <span className="ms-1 d-none d-md-inline">Produits</span>
      </NavLink>
      <NavLink to={`/dashboard/categories`} className="d-block">
        <GiBookPile /> <span className="ms-1 d-none d-md-inline">Produits</span>
      </NavLink> <NavLink to={`/dashboard/fornisseur`} className="d-block">
        <GiBookPile /> <span className="ms-1 d-none d-md-inline">Produits</span>
      </NavLink> <NavLink to={`/dashboard/clients`} className="d-block">
        <GiBookPile /> <span className="ms-1 d-none d-md-inline">Produits</span>
      </NavLink>
      <NavLink to='/dashboard/commande' className="d-block">
        <GiBookPile /> <span className="ms-1 d-none d-md-inline">Tache</span> 12
      </NavLink>
    </div>
  );
};

export default SideBar;
