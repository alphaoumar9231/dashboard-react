import "./Cards.css";
import React, { useEffect, useState } from "react";
import images from '../../images/pommes.jpg';
// import axios from "axios";
import { MdAddShoppingCart } from "react-icons/md";
import { Col, Container, Row } from "react-bootstrap";
// import Swal from "sweetalert2";
// import getLocStorage from "../ExportComponent";


function Cards({ setData }) {


  // useEffect(() => {
  //   fetchProducts()
  // }, [])

  // const fetchProducts = async () => {
  //   await axios.get(`http://localhost:8000/api/produits`).then(({ data }) => {
  //     setProducts(data)
  //   })
  // }

  // const [products, setProducts] = useState([])

  // const addlocalStrage = (data) => {
  //   const storedProduits = localStorage.getItem("produits");
  //   const produits = JSON.parse(storedProduits) || [];
  //   produits.push({
  //     id: data.id,
  //     nom: data.nom,
  //     image: data.images,
  //     prix: data.prix,
  //     categorie: data.id_category,
  //   });
  //   localStorage.setItem("produits", JSON.stringify(produits));
  //   Swal.fire({
  //     icon: "success",
  //     text: "Article ajouté au panier!",
  //     timer: 2000,
  //     timerProgressBar: true,
  //     showConfirmButton: false,
  //   });
  //   getLocStorage(setData);
  // };


  const products = [
    { images: images, description: 'Voici la description du produit', nom: 'Banane', prix: 200 },
    { images: images, description: 'Voici la description du produit', nom: 'Banane', prix: 200 },
    { images: images, description: 'Voici la description du produit', nom: 'Banane', prix: 200 },
    { images: images, description: 'Voici la description du produit', nom: 'Banane', prix: 200 },
    { images: images, description: 'Voici la description du produit', nom: 'Banane', prix: 200 },
    { images: images, description: 'Voici la description du produit', nom: 'Banane', prix: 200 },
    { images: images, description: 'Voici la description du produit', nom: 'Banane', prix: 200 },
  ]





  return (
    
      <Container>
        <Row>

          {products.map((item, index) => {
            return <Col xs={6} sm={4} key={index} className="p-1 ">
              <div className=" cardProduit">
                <div className="proj-imgbx">
                  <img src={item?.images} className="img-fluid" alt="Mes cards de produits" />
                  <div className="proj-txtx ">
                    <span>{item?.description}</span>
                    <div className="d-flex justify-content-center pt-3 align-items-center">
                      <button className="btn btn-md d-flex" ><MdAddShoppingCart /> <span> Ajouter </span>   </button>
                    </div>
                  </div>
                </div>
                <div className="py-1 mytextC">
                  <h6 className="text-center fw-semibold text-uppercase">{item?.nom}</h6>
                  <div className="d-flex justify-content-between px-2">
                    <p className="fw-semibold">Prix :</p> <strong>{item.prix} fcfa</strong>
                  </div>
                </div>
              </div>
            </Col>
          })}</Row>
      </Container>

  
  );
};

export default Cards;
