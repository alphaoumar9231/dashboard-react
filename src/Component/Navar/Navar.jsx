import "./Navar.css";
import React, { useEffect, useState } from "react";
import avatar from '../../images/logo.png';
import ProfilNavBar from "../ProfilNavBar/ProfilNavBar";
import { GoThreeBars } from "react-icons/go";

function Navar() {

  const sidebar = document.getElementsByClassName("side-bar");
  const lyt = document.getElementsByClassName('layout-nav-side')
  const [show, setShow] = useState(false);


  const handleClose = () => {
    if (show === false) {
      setShow(true);
    }
    else { setShow(false); }
  }

  useEffect(() => {
    if (show === true) {
      sidebar[0].classList.add("afficher");
      lyt[0].classList.remove("bien22");
    }
    else {
      sidebar[0].classList.remove("afficher");
      lyt[0].classList.add("bien22");
    }
  })

  return (
    <div className="navar" id="navar">
      <nav class="navbar navbar-expand-lg ">
        <div class="container-fluid d-flex justify-content-between align-items-center">
          <div className="d-flex justify-content-between align-items-center">
            <div>
              <a class="navbar-brand" href="#">
                <img src={avatar} alt="" className="logoNavbar" />
              </a>
            </div>
            <div>
              <button className="btnContAffi pb-3" onClick={handleClose}> <GoThreeBars className="btnAffi" /> </button>
            </div>

          </div>
          <div class="d-flex justify-content-between" id="">
            <ProfilNavBar />
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navar;