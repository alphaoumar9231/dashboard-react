import "./Layout.css";
import React from "react";
import Navar from '../Navar/Navar'
import SideBar from '../SideBar/SideBar'
import { Outlet } from "react-router-dom";

function Layout() {
  return (
    <div className="layout layout-nav-side position-relative">
      <Navar />
      <div className="d-flex position-relative">
        <SideBar className='d-none'/>
        <div className="outlet lNS-droit " >
          <Outlet/>
        </div>
      </div>
    </div>
  );
};

export default Layout;
